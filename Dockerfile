FROM python:3.8
WORKDIR /app
# comes from volumes, for MANAGER only
RUN mkdir -p data
ADD req.txt .
RUN python3 -m pip install -r req.txt
ADD *.py ./
# Command to be overriden by the running env!
CMD ['python3', 'manager.py']