# Web-crawler #

This repo is for a job test task to verify my Python knowlegde. By Dmytro Povesma

## Requirements

Should be ready for additional features (or/and scaling).

### Crawling

Web-crawler in Python, that is able to download http(s) URLs and (it it is HTML) find all links in an <a> tag (excluding dynamic / script generated / 
hidden links or references, buttons etc), save them under the name identical to URL, and crawl ti the given crawl depth, downloading all the HTMLs linked to the main URL and the nested URLs.

If interrupted before ending, it should resume crawling with a reasonble re-doing.

### Analyzing

Count main URL domain / all unique domains ratio for each URL.

### Reporting

`URL [tab]   depth [tab]  domain_ratio` as a tab separated file, for each fetched URL.

## Initial design / Ideas

### Bottlenecks

Single URL fetching is trivial. https://stackoverflow.com/questions/843392/python-get-http-headers-from-urllib2-urlopen-call (**Challenge: stop on wrong content-type**)

Sanitizing URL -> legal filename is trivial (https://stackoverflow.com/questions/27253530/save-url-as-a-file-name-in-python)

HTML parsing is trivial (https://stackoverflow.com/questions/15926142/regular-expression-for-finding-href-value-of-a-a-link/15926317)

Crawl depth limiting is trivial (exclude already scraped URLs to avoid circular refs).

Extract domain name is trivial (https://www.simplified.guide/python/get-host-name-from-url)

Analyzing the number of domain names is trivial (count them before discarding the duplicates)

Report generating and saving is trivial

**Caching / resuming is the most critical requirement, and it defines the archetecture. Why? All around it: load unfinished jobs on startup, save job progress and results**

### Architecture

* Some pub/sub for the persistence (with a persistence feature): experts in persistence; better than a DB as pub/sub generate evens on changes
* "Microservices" for:
    - Job(s) manager: pubs initial URL, crawl depth, receives and stores report from other services (completion of fetch, completion of
    - Fetcher: Fetch URL and return the HTML [worker]
    - Analyzer: Parse HTML and return the list of URL to the job [worker]
	- Reporter: Writes a report file / stores other data [worker]
* Discovery responder for every "microservice"
* DB for persistence of active/completed jobs and results

### Data flow

![Data flow](/images/Web-crawler-2021.png)

## Implementation

As designed initially :) except:

1. Reporting is done in Manager (for dev time sake)
1. No grace Shutdown
1. Restore after crash not checked good enough, but should work


Message bus: Rabbit MQ

DB: Local MongoDB-like - **MontyDB** (data is stored localy, in JSON-file)

Workers per fetcher / parser: 3 (hardcoded)

Deployment: Docker-compose + single instance of `manager.py` from CLI

![Data flow](/images/Web-crawler-2021-2.png)


## Flaws

1. No pretty logging
1. Poor tests
1. NO metrics exposed
1. Messy stop / restart
1. Parameters from "Data flow" diagram not implemented
1. No global URL deduplication
1. Lots of TODOs in the code
  
## How to test

`python3 -m unittest`

## How to run

### Configuration

env var `rabbitmq_host` defines RabbitMQ host for `manager` (`localhost` by deafult, so re-define if needed)

### Prepare

Build the image:

`docker build -t light .`

Create a persistense directory for RabbitMQ:

`mkdir data`

Create a persistense directory for downloaded files and reports:

`mkdir data`

Install required Python modules:

`python3 -m pip install -r req.txt`

### Run

Bring up RabbitMQ + Fetcher + Parser (+Reporter, which is not used via MQ now):

`docker-compose up -d`

Run the crawler core to start crawling:

`python3 ./manager.py http://www.cnn.com/ 3` - CLI params: url, crawling depth

If the process has been interrupted - run the core without parameters.

`python3 ./manager.py`

Stop with Ctrl+C

#### Alternative manager run with Docker

As `manager.py` is packed in the Docker image, you can run it like as a part of
docker-compose (edit `docker-compose.yaml` respectively and set `entrypoint`), or
with CLI:

`docker run --network web-crawler_light -v $(pwd)/files:/app/files -e rabbitmq_host=rabbitmq --rm --name manager light python3 -u manager.py http://www.google.co.il/ 2`

## TODOs

* fix flaws
* push-and-forget mode (API / Publish to MQ)
