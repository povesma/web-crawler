import urllib.request
from urlopener import UrlOpener
from urllib.error import URLError
import sys, os, json
import pika
from routine import MQ
import socket
import bs4
from http.client import RemoteDisconnected

#TODO: proper logging

INTERNAL_WORKERS = 3

follow_redirects = True
unsafe = False
TIMEOUT = 7 #seconds
def fetch(url):
	html = None
	status_code = None
	final_url = url
	urlOpener = UrlOpener()
	OPENER = urlOpener.getOpener(follow_redirects, not unsafe)  # to follow? , safe (check cert)?
	request = urllib.request.Request(url, None, method='GET')
	url_req_result = OPENER.open(request, data = None, timeout = TIMEOUT)
	if url_req_result.code >= 200 and url_req_result.code < 300:
		# check content-type
		response_headers = dict(url_req_result.info().items())
		#print("response_headers: {}, keys: {}".format(response_headers, response_headers.keys()))
		# finding content-type header...
		ct_header = None
		u_headers = [x.upper() for x in response_headers]
		ct_index = u_headers.index("CONTENT-TYPE")
		ct_header = list(response_headers.keys())[ct_index]
		if not ct_header:
			print ("No content-type header!")
			return None
		content_type = response_headers[ct_header].split(";")[0] #handling things like "text/html; charset=ISO-8859-1"
		if content_type.lower() == "text/html": #continue! TODO: Use HEAD / interrupted load to avoid excess traffic
			html = url_req_result.read() #.decode("UTF-8")
			final_url = url_req_result.geturl()
			status_code = url_req_result.code
			print("final_url: {}, html len: {}, html type: {}, code: {}".format(final_url, len(html), type(html), status_code))
		else:
			print ("Wrong content-type: {}".format(content_type))
	else:
		print ("Error code: {}".format(url_req_result.code))
		return (None, None, None, None) # error
	return (html, final_url, final_url != url, status_code) # 3rd var - has been redirected

FETCHER_Q_NAME = "fetcher" # we subscribe here
PARSER_Q_NAME = "parser" # we publish here the fetched HTML for parsing
MANAGER_SAVEFILE_Q_NAME = "manager_savefile" # we publish here the fetched HTML for saving

def worker(channel, method, properties, body):
	try:
		j = json.loads(body.decode())
		print ("j: {}".format(j))
		if  "job_id" in j and "url" in j and "level" in j :
			try:
				#print("Fetching for {}".format(j["url"]))
				try:
					(html, final_url, redirected, status_code) = fetch(j["url"])
					j["html"] = html
				except URLError as fe:
					print("Fetch Exception, URL unavailable <{}>, Acking: {}"
						.format(j["url"], fe))
				except socket.timeout as se:
					print("Fetch Exception, socket.timeout <{}>, Acking: {}"
						.format(j["url"], se))
				except RemoteDisconnected as re:
					print("Fetch Exception, RemoteDisconnected <{}>, Acking: {}"
						.format(j["url"], re))
				if not j.get("html"):
					j["html"] = None
				else:
					# use BeautifulSoup for safe conversion to text:
					j["html"] = str(bs4.BeautifulSoup(j["html"], 'html.parser'))
					print("HTML len for {} is {}".format(j["url"], len(j["html"])))
				body = json.dumps(j)
				# publish back: to Parser
				pub_res = channel.basic_publish(exchange = '',
						routing_key = PARSER_Q_NAME,
						body = body,
						properties=pika.BasicProperties(
							delivery_mode=2,  # make message persistent
						)
					)
				# publish back: to File saver
				del j["level"]
				pub_res = channel.basic_publish(exchange = '',
						routing_key = MANAGER_SAVEFILE_Q_NAME,
						body = body,
						properties=pika.BasicProperties(
							delivery_mode=2,  # make message persistent
						)
					)
			except Exception as e:
				print ("Fetch: problem fetching / publishing: {}. NOT ACKED".format(e))
				channel.basic_reject(delivery_tag = method.delivery_tag)
				raise e
			# ack in the end
		else:
			print("Fetch: job_id, url and level fields are mandatory! Acking.")
	except json.decoder.JSONDecodeError as e:
		print("Fetch: wrong message format! Acknowledging anyway.")

	print(" Fetch MQ Done. Acking.")
	channel.basic_ack(delivery_tag = method.delivery_tag)

def connect_mq(params):
	print("MQ connection params {}".format(params))
	connection = pika.BlockingConnection(pika.ConnectionParameters(
		host = params["host"], connection_attempts = 7, retry_delay = 3)
	)
	return connection

def get_mq_channel(connection, persistent = True, ):
	channel = connection.channel()
	channel.queue_declare(queue = MANAGER_SAVEFILE_Q_NAME, durable = persistent)
	channel.queue_declare(queue = PARSER_Q_NAME, durable = persistent)
	channel.queue_declare(queue = FETCHER_Q_NAME, durable = persistent)
	channel.basic_qos(prefetch_count = INTERNAL_WORKERS) # workers count
	channel.basic_consume(queue = FETCHER_Q_NAME, on_message_callback = worker)
	return channel


if __name__ == '__main__':
	params = {}
	# init rabbitMQ:
	params["host"] = os.environ.get("rabbitmq_host") or "localhost"
	params["port"] = os.environ.get("rabbitmq_port") or "5672" # not used yet
	connection = connect_mq(params)
	channel = get_mq_channel(connection)
	# TODO: graceful shutdown
	#channel.start_consuming() # TODO: use consume generator instead https://stackoverflow.com/questions/32220057/interrupt-thread-with-start-consuming-method-of-pika
	C = MQ(channel)
	C.start()
	C.join()
