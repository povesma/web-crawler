from routine import Job, Result
from threading import Thread
from reporter import analyze_result, analyze_job
from parser import parse_html
from fetch import fetch
import sys, os, json
import pika
from urllib.parse import quote
from routine import MQ
from montydb import MontyClient


INTERNAL_WORKERS = 3
class Manager(object):
	"""Handles web crawling process"""
	def __init__(self, channel, params = {}):
		super(Manager, self).__init__()
		self.params = params
		self.reports = {}
		self.channel = channel # RabbitMQ channel
		self.db = MontyClient("./db/montydb")
		self.jobs = Job.load_all(self.db)
		# TODO: load active jobs from DB?
		channel.basic_consume(queue = MANAGER_SAVEFILE_Q_NAME, 
			on_message_callback = self.save_file)

		channel.basic_consume(queue = MANAGER_RESULT_Q_NAME, 
			on_message_callback = self.q_add_result)

		# TODO: add along with MQing reporting
		#channel.basic_consume(queue = MANAGER_REPORT_Q_NAME, 
		#	on_message_callback = self.q_add_report)

	def save_file(self, channel, method, properties, body):
		fname = None
		try:
			j = json.loads(body.decode())
			if "html" in j and "job_id" in j and "url" in j:
				if j["html"]: #not empty
					try:
						os.makedirs("files", exist_ok = True)
						os.makedirs("{}/{}".format("files", j["job_id"]), exist_ok = True)
						# TODO: Proper workaround for long file names
						fname = r"{}/{}/{}.html".format("files", j["job_id"], url2filename(j["url"])[:64])
						file = open(fname, "w")
						file.write("{}".format(j["html"])) # TODO: big chunks? .encode("utf-8")
						file.close()
					except Exception as e:
						print ("Manager save file: problem saving: {}. NOT ACKED (MQ will retry)".format(e))
						channel.basic_reject(delivery_tag = method.delivery_tag)
						return
				# ack in the end
			else:
				print("Manager save file: html, job_id and url fields are mandatory! Acking.")
		except json.decoder.JSONDecodeError as ex:
			print("Manager save file: wrong message format! Acknowledging anyway.", ex)
		print(" Manager save file: {}. MQ Done. Acking.".format(fname))
		channel.basic_ack(delivery_tag = method.delivery_tag)

	def q_add_result(self, channel, method, properties, body):
		try:
			j = json.loads(body.decode())
			if "level" in j and "job_id" in j and "nested_urls" in j and "url" in j:
				res = Result.deserialize(j)
				try:
					self.process_result(j["job_id"], res) # Result
				except Exception as e:
					print ("q_add_result: problem adding result: {}. NOT ACKED (MQ will retry)".format(e))
					channel.basic_reject(delivery_tag = method.delivery_tag)
					return
				# ack in the end
			else:
				print("q_add_result: nested_urls, job_id, level and url fields are mandatory! Acking.")
		except json.decoder.JSONDecodeError as ex:
			print("q_add_result: wrong message format! Acknowledging anyway.", ex)
		# print("q_add_result: MQ Done. Acking.")
		channel.basic_ack(delivery_tag = method.delivery_tag)

	def start_job(self, job: Job):
		self.jobs.add(job) # add if it's not there
		# First step
		runner = Depth(job.URL, job.ID, 1, self.process_result, self.getChannel()) # job and callback for results
		job.inc_field("started_count")
		runner.start()

	# pika is not thread-save, each thread needs a connection
	def getChannel(self):
		connection = connect_mq(params)
		return get_mq_channel(connection)
	# this one to be called as PubSub callback
	def process_result(self, job_id: str, r: Result): # single url result
		job = self.find_job(job_id)
		if not job:
			print("  process_result: FAILURE: cant find job: {}".format(job_id))
			return
		job.inc_field("done_count")# + 1 and update DB
		if r:
			# save to DB
			pass
			# add result to the job
			job.add_result(r) # adds to job.results
		else: # Probably, error
			print("process_result: empty nested_urls: {} / {}"
					.format(job.done_count, job.started_count))
		# if not reached depth - start workers for 
		if r and job.depth > r.level:
			for n in set(r.nested_urls): # deduplication to avoid double fetching from the same page
				d = Depth(n, job.ID, r.level + 1, self.process_result, self.getChannel())
				job.inc_field("started_count")# + 1 and update DB
				d.start()
		#if not r or r.level >= job.depth: # if level 1 has no links, it will never go to level 2.
		if job.started_count == job.done_count:
			print("Finished for job: {}, depth: {}, results count: {}"
				.format(job.ID, job.depth, len(job.results)))
			# start reporting - TODO: move to MQ
			for res in job.results:
				rep = Reporter(job_id, res, self.add_report)
				rep.start()
				rep.join() # wait for completing
			job.finish()
			tsv = "{}".format("\n".join(self.reports[job.ID]))
			os.makedirs("files", exist_ok = True)
			fname = "files/{}.tsv".format(job.ID)
			file = open(fname, "w")
			file.write(tsv)
			print("\nCompleted. Saved to {}".format(fname))
		else:
			print("Final depth level. Waiting for jobs to complete: {} / {}"
				.format(job.done_count, job.started_count))

	# this one to be called as PubSub callback
	def add_report(self, job_id: str, report: str):
		job = self.find_job(job_id)
		if not job:
			print("  REPORT FAILURE: cant find job: {}".format(job_id))
			return
		if job_id not in self.reports:
			self.reports[job.ID] = []
		self.reports[job.ID].append(report)
		# check if all jobs are finished


	# find job by ID (very slow search!)
	def find_job(self, job_id):
		j = [l for l in self.jobs if l.ID == job_id]
		return j[0] if len(j) > 0 else None

# PubSub Mockup
class Depth(Thread):
	"""Threaded URL fetching"""
	def __init__(self, url: str, job_id: str, level: int, callback, channel):
		super().__init__()
		self.job_id = job_id
		self.level = level
		self.url = url
		self.callback = callback
		self.channel = channel

	def run(self):
		print("Worker Started! URL: {}".format(self.url))
		res = None
		try:
			# TODO: Send to MQ instead of sync call
			pub_res = self.channel.basic_publish(exchange = '',
					routing_key = FETCHER_Q_NAME,
					body = json.dumps({"url": self.url, "job_id": self.job_id,
						"level": self.level}),
					properties=pika.BasicProperties(
						delivery_mode=2))  # make message persistent
			return
			(html, final_url, is_redirect, code) = fetch(self.url)
			if html:
				# publish for parsing
				n = parse_html(html) # list of nested urls
				res = Result(URL = self.url, # or final_url?
						job_id = self.job_id, 
						level = self.level, 
						nested_urls = n)
			else:
				print("Manager Depth run: Empty response for {}".format(self.url))
		except Exception as ex:
			print("Manager Depth run: Some exception: ", ex)
		#self.callback(self.job_id, res)
		self.channel.close()

	def parsed():
		pass

# PubSub Mockup
class Reporter(Thread):
	"""Threaded reporter"""
	def __init__(self, job_id: str, result: Result, callback):
		super().__init__()
		self.job_id = job_id
		self.result = result
		self.callback = callback

	def run(self):
		a = analyze_result(self.result)
		self.callback(self.job_id, a)

def url2filename(url):
	return quote(url.replace('/', '~').replace(':', '_'), "~_")

def args_depth(depth = None):
	DEPTH = depth or 2
	if len(sys.argv) > 2:
		try:
			DEPTH = int(sys.argv[2])
		except Exception as e:
			print ("Second parameter is not a number! Using default: ", DEPTH, e)
		sys.argv[1]
	else:
		print ("No depth specified. Usinf default:", DEPTH)
	return DEPTH

params = {}
connection = None
channel = None

FETCHER_Q_NAME = "fetcher" # we publish here
PARSER_Q_NAME = "parser" # we publish here
REPORTER_Q_NAME = "reporter" # we publish here
MANAGER_RESULT_Q_NAME = "manager_result" # we subscribe here for parsed results
MANAGER_REPORT_Q_NAME = "manager_report" # we subscribe here for prepared reports
MANAGER_SAVEFILE_Q_NAME = "manager_savefile" # we subscribe here for saving downloaded file to the disk


def connect_mq(params):
	# print("MQ connection params {}".format(params))
	connection = pika.BlockingConnection(pika.ConnectionParameters(host = params["host"]))
	return connection

def get_mq_channel(connection, persistent = True, ):
	channel = connection.channel()
	channel.queue_declare(queue = MANAGER_SAVEFILE_Q_NAME, durable = persistent)
	channel.queue_declare(queue = MANAGER_RESULT_Q_NAME, durable = persistent)
	channel.queue_declare(queue = MANAGER_REPORT_Q_NAME, durable = persistent)
	channel.queue_declare(queue = PARSER_Q_NAME, durable = persistent)
	channel.queue_declare(queue = FETCHER_Q_NAME, durable = persistent)
	channel.queue_declare(queue = REPORTER_Q_NAME, durable = persistent)
	channel.basic_qos(prefetch_count = INTERNAL_WORKERS) # workers count
	return channel

params = {}
if __name__ == '__main__':

	# init rabbitMQ:
	params["host"] = os.environ.get("rabbitmq_host") or "localhost"
	params["port"] = os.environ.get("rabbitmq_port") or "5672" # not used yet
	connection = connect_mq(params)
	channel = get_mq_channel(connection)
	#channel.start_consuming()

	# start the job
	M = Manager(channel)
	if len(sys.argv) > 2:
		J = Job(URL = sys.argv[1] or "https://freebsd.org", depth = args_depth(2), db = M.db)
		M.start_job(J)
	# start consuming from MQ
	 # TODO: use consume generator instead https://stackoverflow.com/questions/32220057/interrupt-thread-with-start-consuming-method-of-pika
	C = MQ(channel)
	C.start()
	C.join()