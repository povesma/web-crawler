from typing import Dict, List
import re, sys, json, os
from bs4 import BeautifulSoup # Sophisticated HTML operation https://www.crummy.com/software/BeautifulSoup/bs4/doc/
import pika
from json.decoder import JSONDecodeError
from routine import MQ

INTERNAL_WORKERS = 3

def parse_html(html) -> List[str]:
	#Using BeautifulSoup. Alternatively - use regexps or Selenium for complex evaluations
	soup = BeautifulSoup(html, 'html.parser')
	links = []
	# relative URLs are ignored as of now. TODO: support relative URLs
	# TODO: deduplication? Here or in the crawler?
	for link in soup.findAll('a', attrs={'href': re.compile("^https?://")}):
		links.append(link.get('href'))
	return links

def worker(channel, method, properties, body):
	#print(" [x] Received {}, {}, {}".format(body.decode(), method, properties))

	try:
		j = json.loads(body.decode())
		if "html" in j and "job_id" in j and "url" in j and "level" in j:
			try:
				res = None
				if j["html"]:
					res = parse_html(j["html"])
				# publish back
				del j["html"]
				j["nested_urls"] = res
				pub_res = channel.basic_publish(exchange='', routing_key = MANAGER_RESULT_Q_NAME,
						body = json.dumps(j),
						properties=pika.BasicProperties(
							delivery_mode=2,  # make message persistent
						)
					)
			except Exception as e:
				print ("Parse: problem parsing / publishing: {}. NOT ACKED".format(e))
				return
			# ack in the end
		else:
			print("html, job_id, url and level fields are mandatory! Acking.")
	except json.decoder.JSONDecodeError as e:
		print("Parse: wrong message format! Acknowledging anyway.")

	print(" Parsing Done. Acking.")
	channel.basic_ack(delivery_tag = method.delivery_tag)


params = {}
connection = None

PARSE_Q_NAME = "parser" # we subscribe here
MANAGER_RESULT_Q_NAME = "manager_result" # we publish here

def connect_mq(params):
	print("MQ connection params {}".format(params))
	connection = pika.BlockingConnection(pika.ConnectionParameters(
		host = params["host"], connection_attempts = 7, retry_delay = 3)
	)
	return connection

def get_mq_channel(connection, persistent = True):
	channel = connection.channel()
	channel.queue_declare(queue = MANAGER_RESULT_Q_NAME, durable = persistent)
	channel.queue_declare(queue = PARSE_Q_NAME, durable = persistent)
	channel.basic_qos(prefetch_count = INTERNAL_WORKERS) # workers count
	channel.basic_consume(queue = PARSE_Q_NAME, on_message_callback = worker)
	return channel

if __name__ == '__main__':
	params["host"] = os.environ.get("rabbitmq_host") or "localhost"
	params["port"] = os.environ.get("rabbitmq_port") or "5672" # not used yet
	connection = connect_mq(params)
	channel = get_mq_channel(connection)
	# TODO: graceful shutdown
	#channel.start_consuming() # TODO: use consume generator instead https://stackoverflow.com/questions/32220057/interrupt-thread-with-start-consuming-method-of-pika

	C = MQ(channel)
	C.start()
	C.join()