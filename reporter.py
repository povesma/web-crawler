from routine import Job
from routine import Result
import urllib.parse
import re, sys, json, os
import pika
from json.decoder import JSONDecodeError
from routine import MQ

# Does not work with DB?

def analyze_job(job: Job, file = None): # if file present - write to the file
	out = []
	if job.results: # array of all result
		for r in job.results: # result for a single URL like {job_id, URL, level, [nested_urls]}
			# Async to prevent thread blocking?
			if file:
				# write to file to save memory
				pass
			else:
				out.append(analyze_result)
			# Too long list? DB?
	# sort if needed
	return "\n".join(out)

def analyze_result(result: Result):
	url_domain = extract_domain(result.URL)
	other_domains = dict()
	own_domains_count = 0
	for n in result.nested_urls:
		# extract domain name:
		d = extract_domain(n)
		if url_domain == d:
			own_domains_count += 1
		else:
			inc_dict_val(other_domains, d)
	#print("own: {}, other_domains: {}".format(own_domains_count, other_domains))
	total = (sum(other_domains.values()) + own_domains_count)

	rate = own_domains_count / total if total > 0 else 0
	return "{}\t{}\t{}".format(result.URL, result.level, rate)

def extract_domain(url):
	return urllib.parse.urlparse(url).netloc

def inc_dict_val(d, k): #ensures the key exsists in dict and increases it by 1
	d[k] = d[k] + 1 if d.get(k) else 1

params = {}
connection = None

REPORT_Q_NAME = "reporter" # we subscribe here
MANAGER_REPORT_Q_NAME = "manager_report" # we publish here


def worker(channel, method, properties, body):
	try:
		j = json.loads(body.decode())
		#print ("j: {}".format(j))
		if  "job_id" in j and "url" in j and "level" in j and "html" in j:
			try:
				r = Result.deserialize(j)
				try:
					j["nested_urls"] = analyze_result(r)
				except Exception as fe:
					print("Report Exception, Acking:", fe)
				body = json.dumps(j)
				# publish back: to Manager
				pub_res = channel.basic_publish(exchange = '',
						routing_key = MANAGER_REPORT_Q_NAME,
						body = body,
						properties=pika.BasicProperties(
							delivery_mode=2,  # make message persistent
						)
					)
			except Exception as e:
				print ("Reporter: problem parsing / publishing: {}. NOT ACKED".format(e))
				raise e
			# ack in the end
		else:
			print("Reporter: job_id, url and level fields are mandatory! Acking.")
	except json.decoder.JSONDecodeError as e:
		print("Reporter: wrong message format! Acknowledging anyway.")

	print(" Reporter MQ Done. Acking.")
	channel.basic_ack(delivery_tag = method.delivery_tag)


def connect_mq(params):
	print("MQ connection params {}".format(params))
	connection = pika.BlockingConnection(pika.ConnectionParameters(
		host = params["host"], connection_attempts = 7, retry_delay = 3)
	)
	return connection

def get_mq_channel(connection, persistent = True):
	channel = connection.channel()
	channel.queue_declare(queue = MANAGER_REPORT_Q_NAME, durable = persistent)
	channel.queue_declare(queue = REPORT_Q_NAME, durable = persistent)
	channel.basic_qos(prefetch_count = 1) # workers count
	channel.basic_consume(queue = REPORT_Q_NAME, on_message_callback = worker)
	return channel

if __name__ == '__main__':
	params["host"] = os.environ.get("rabbitmq_host") or "localhost"
	params["port"] = os.environ.get("rabbitmq_port") or "5672" # not used yet
	connection = connect_mq(params)
	channel = get_mq_channel(connection)
	# TODO: graceful shutdown
	#channel.start_consuming() # TODO: use consume generator instead https://stackoverflow.com/questions/32220057/interrupt-thread-with-start-consuming-method-of-pika
	C = MQ(channel)
	C.start()
	C.join()
