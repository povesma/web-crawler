from typing import Dict, List
import json
import uuid
import pika
from threading import Thread

class Job(object):
	def __init__(self, ID = None, URL = None, depth = 1, nested_urls = [], db = None):
		self.ID = ID or randomID()
		print("NEW JOB: {}".format(self.ID))
		self.params = {} # fetching rules, for future use
		self.URL = URL # Root URL
		self.depth = depth # Levels limit; 1 - just root URL
		#self.nested_urls = nested_urls # list of lists strings 
		self.results = [] # list of crawling results for each level ( 0 - root url)
		self.started_count = 0
		self.done_count = 0
		self.client = db
		self.db = self.client.get_database("crawler")
		self.coll = self.db.get_collection("jobs")

	def inc_field(self, field): # fiels - started_count or done_count
		if field not in ["started_count", "done_count"]:
			print("ERROR: wrong inc field name!")
			return
		val = self.__getattribute__(field) + 1
		self.__setattr__(field, val)

		#save to DB
		try:
			upd_res = self.coll.update_one(
				{"ID": self.ID, "URL": self.URL, 
				"depth": self.depth},
				{"$set":{"{}".format(field): val}}, upsert = True)
			print("{} increased! {}".format(field, val))
			return upd_res
		except Exception as e:
			print("Error: {} Increase failed DB err: {}".format(field, e))
			raise e

	def add_result(self, result): # result of type Result
		self.results.append(result) # in mem

		#save to DB
		try:
			r = result.serialize()
			val = self.coll.update_one(
				{"ID": self.ID, "URL": self.URL, 
				"depth": self.depth},
				{"$push": {"results": r}, "$set":{"started_count": self.started_count},
				"$set":{"done_count": self.done_count}}, upsert = True)
			print("Added res to DB: {},".format(self.ID))
			return val
		except Exception as e:
			print("{} update results in DB err: {}".format(self.ID, e))
			raise e

	def finish(self): # result of type Result
		try:
			self.coll.update_one({"ID": self.ID, "URL": self.URL, "depth": self.depth},
				{"$set": {"finished": True}}, upsert = True) # timestamp?
			print("Job {} marked finished!".format(self.ID))
		except Exception as e:
			print("{} finishing Job in DB err: {}".format(self.ID, e))

	def load_one(self, db, job_id): # load the job from DB
		# read one job

		res = self.coll.find({"ID": job_id})
		try:
			j = res.next()
			self.__dict__.update((k, j[k]) for k in set(j).intersection(self.__dict__))
		except Exception as e:
			print("{} is not in DB: {}".format(job_id, e))
			return None
		# read the results
		#Result.load(db, self.ID)
		
	def load_all(db): # load all the jobs from DB (but not finished)
		res = set()
		J = Job(db = db)
		f = J.coll.find({"finished": {"$ne": True}})
		try:
			for j in f:
				J = Job(db = db)
				J.__dict__.update((k, j[k]) for k in set(j).intersection(J.__dict__))
				res.add(J)
		except Exception as e:
			print("ERROR: Loading all from DB err: ".format(e))
		print ("Loaded: {} not-stopped jobs".format(len(res)))
		return res

class Result(object): # single URL processing result
	"""Result of single URL processing like {job_id, url, level, [nested_urls]}"""
	def __init__(self, URL: str, job_id: str = None, level: int = None, nested_urls = []):
		self.job_id = job_id
		self.URL = URL
		self.nested_urls = nested_urls or []
		self.level = level
		self.html = None # URLs html

	# static - returns an array or Result [ Result ]
	def load(db, job_id: str = None, level: int = None) -> List[object]: # load the job from DB
		# read the results
		if not job_id or type(job_id) is not str:
			raise Exception("load Result from DB: job_id not defined")

		if level and ((type(level) is not int) or level < 1):
			raise Exception("load Result from DB: level not defined or wrong: {}".format(level))
		res = []
		return res

	def serialize(self):
		s = {"job_id": self.job_id, "url": self.URL, "nested_urls": self.nested_urls,
				"level": self.level, "html": self.html}
		return json.dumps(s)

	def deserialize(j: dict):
		try:
			r = Result(j.get("url"), job_id = j.get("job_id"), 
				level = j.get("level"), nested_urls = j.get("nested_urls"))
			r.html = j.get("html")
			return r
		except Exception as e:
			print("Result deserialize error: {}".format(e))
			return None

class MQ(Thread):
	"""docstring for ClassName"""
	def __init__(self, channel):
		super(MQ, self).__init__()
		self.channel = channel
		self.isAlive = True

	def run(self):
		while self.isAlive:
			print("Staring MQ consuming...")
			try:
				self.channel.start_consuming()
			except Exception as e:
				print("MQ run err. Continue")
				raise e
		
def randomID():
	return str(uuid.uuid4())