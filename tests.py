import unittest
from routine import Job, Result
from reporter import analyze_result, analyze_job
from parser import parse_html
from fetch import fetch
import pika
import json, os, sys
from montydb import MontyClient

class TestJobDB(unittest.TestCase):
	def test_DB_update(self):
		print("Test DB Update")
		client = MontyClient("./db1")
		j_id = "1111-33333-11111111"
		r = Result("URL_HERE", j_id, 1, ["eeeee", "pppppp"])
		v = Job(j_id, "URL_HERE", db = client)
		u = v.add_result(r)
		self.assertEqual(u.acknowledged, True)

	def test_DB_load_all_jobs(self):
		client = MontyClient("./db1")
		u = Job.load_all(client)
		self.assertEqual(type(u), list)

class TestMQ(unittest.TestCase):
	params = {}
	connection = None
	channel = None

	FETCHER_Q_NAME = "fetcher" # we publish here
	PARSER_Q_NAME = "parser" # we publish here
	REPORTER_Q_NAME = "reporter" # we publish here
	MANAGER_RESULT_Q_NAME = "manager_result" # we subscribe here for parsed results
	MANAGER_REPORT_Q_NAME = "manager_report" # we subscribe here for prepared reports
	MANAGER_SAVEFILE_Q_NAME = "manager_savefile" # we subscribe here for saving downloaded file to the disk

	def connect_mq(params):
		print("MQ connection params {}".format(params))
		connection = pika.BlockingConnection(pika.ConnectionParameters(host = params["host"]))
		return connection

	def get_mq_channel(connection, persistent = True, ):
		channel = connection.channel()
		channel.queue_declare(queue = TestMQ.MANAGER_RESULT_Q_NAME, durable = persistent)
		channel.queue_declare(queue = TestMQ.MANAGER_REPORT_Q_NAME, durable = persistent)
		channel.queue_declare(queue = TestMQ.PARSER_Q_NAME, durable = persistent)
		channel.queue_declare(queue = TestMQ.FETCHER_Q_NAME, durable = persistent)
		channel.queue_declare(queue = TestMQ.REPORTER_Q_NAME, durable = persistent)
		channel.basic_qos(prefetch_count = 1) # workers count
		return channel

	def test_MQ(self):
		connection = TestMQ.connect_mq({"host": "localhost"})
		channel = TestMQ.get_mq_channel(connection)
		channel.basic_publish(exchange='', routing_key = TestMQ.FETCHER_Q_NAME,
			body = json.dumps({
						"url": "https://www.gigicosmetics.com",
						"job_id": "sample_job_id",
						"level": 1
					}),
			properties=pika.BasicProperties(delivery_mode=2))  # make message persistent)

class TestE2E(unittest.TestCase):
	def test_single_url(self):
		url = 'https://www.cncf.io/'
		(html, final_url, is_redirect, code) = fetch(url)
		n = parse_html(html) # list of nested urls
		print ("e2e: {}, html len:".format(len(n), len(html)))
		res = Result(URL = url, # or final_url?
				job_id = "some_id", 
				level = 1, 
				nested_urls = n)
		# expected result:
		s = "{}\t{}\t{}".format(url, 1, 0.4292682926829268)
		a = analyze_result(res)
		self.assertEqual(s, a)

class TestFetch(unittest.TestCase):
	def test_fetch_noredir(self):
		url = 'https://www.google.com/'
		(html, final_url, is_redirect, code) = fetch(url)
		print ("Meta: {} == {}, {}".format(url, final_url, is_redirect))
		self.assertEqual(is_redirect, False)

	def test_fetch_redir(self):
		url = 'https://google.com/'
		(html, final_url, is_redirect, code) = fetch(url)
		print ("Meta: {} == {}, {}".format(url, final_url, is_redirect))
		self.assertEqual(is_redirect, True) # We expect edirect here

class TestParse(unittest.TestCase):
	html = """<html><head><title>The Dormouse's story</title></head>
		<body>
		<p class="title"><b>The Dormouse's story</b></p>

		<p class="story">Once upon a time there were three little sisters; and their names were
		<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
		<a href="http://apple.com/lacie" class="sister" id="link2">Lacie</a> and
		<a href="https://m.rabbit.com/tillie" class="sister" id="link3">Tillie</a>;
		and they lived at the bottom of a well.</p>
		"""
	def test_1parse(self):
		s = "{}\n{}\n{}".format('http://example.com/elsie',
					'http://apple.com/lacie',
					'https://m.rabbit.com/tillie')
		self.assertEqual(s, "\n".join(parse_html(TestParse.html)))

	def test_1parse_fail(self):
		s = "{}\n{}\n{}".format('http://example.com/elsie',
					'http://apple.com/lacie',
					'http://m.rabbit.com/tillie') # removed "s" from https
		self.assertNotEqual(s, "\n".join(parse_html(TestParse.html)))

class TestAnalyze(unittest.TestCase):
	def test_1result(self):
		n = ["http://google.com", "https://google.com", 
			"http://apple.com/helpme", "https://apple.com/next?please"]
		URL = "https://apple.com/index.html?lang=pl"
		res = Result(URL = URL, 
				job_id = "some_id", 
				level = 1, 
				nested_urls = n)
		# expected result:
		s = "{}\t{}\t{}".format(URL, 1, 0.5)
		self.assertEqual(s, analyze_result(res))

	def test_1result2(self):
		n = ["http://mail.google.com", "https://google.com", "http://cnn.online/",
			"http://apple.com/helpme", "https://apple.com/next?please", "https://apple.com/another"]
		URL = "https://apple.com/index.html?lang=pl"
		res = Result(URL = URL, 
				job_id = "some_id", 
				level = 1, 
				nested_urls = n)
		# expected result:
		s = "{}\t{}\t{}".format(URL, 1, 0.5)
		self.assertEqual(s, analyze_result(res))

if __name__ == '__main__':
    unittest.main()