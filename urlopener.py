import urllib.parse, urllib.request, urllib.error, ssl

class NoRedirectHandler(urllib.request.HTTPRedirectHandler):
    def __init__ (self):
        super(urllib.request.HTTPRedirectHandler, self).__init__()
    def redirect_request(self, req, fp, code, msg, hdrs, newurl):
        url = req.get_full_url()
        print("NO redirect for {}, throwing: {}".format(url, code))
        return None # do not redirect, HTTPError will be raised
    # def default_open(self, req, fp, code, msg, hdrs, newurl):
    #     url = req.get_full_url()
    #     print ("DEFAULT OPEN {}".format(url))
    def id(self):
        print("NoRedirectHandler")
        return "NoRedirectHandler"


class WithRedirectHandler(urllib.request.HTTPRedirectHandler):
    def __init__ (self):
        super(urllib.request.HTTPRedirectHandler, self).__init__()
        #self.ifRedirect = ifRedirect
    def redirect_request(self, req, fp, code, msg, hdrs, newurl):
        url = req.get_full_url()
        print("With redir for {}. ---> {}, code: {}".format(url, newurl, code))
        return urllib.request.HTTPRedirectHandler.redirect_request(self, req, fp, code, msg, hdrs, newurl)
    # def https_response(self, req, res):
    #     url = req.get_full_url()
    #     print ("DEFAULT OPEN {}, {}".format(url, res.code))
    #     return res
    def id(self):
        print("WithRedirectHandler")
        return "WithRedirectHandler"

class AllErrorHandler(urllib.request.HTTPDefaultErrorHandler):
    def http_error_default(self, req, res, code, msg, hdrs):
        print("DEFAULT Trying to catch HTTP Error: {}".format(code))
        return res

FULL_HTTP_ERROR_LIST = [100, 101, 102, 201, 202, 203, 204, 205, 206, 207, 208, 226, 
    300, 301, 302, 303, 304, 305, 307, 308, 
    400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 
    416, 417, 418, 421, 422, 423, 424, 426, 428, 429, 431, 440, 444, 449, 451, 460, 463, 499, 
    500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511, 520, 521, 522, 523, 524, 525, 526, 527, 530, 599]
class ReqErrorHandler(urllib.request.HTTPErrorProcessor):
    def __init__(self, list_of_errors):
        super(ReqErrorHandler, self).__init__()
        self.list_of_errors = FULL_HTTP_ERROR_LIST if list_of_errors == "all" else list_of_errors
        for e in self.list_of_errors:
            self.__setattr__("http_error_{}".format(e), self.http_error_ANY)
    def http_error_ANY(self, req, res, code, msg, hdrs):
        # print("ANY Trying to catch HTTP Error: {}".format(code))
        return res

class UrlOpener(object):
    def __init__(self):
        print("initialized - Url~Opener")
        noRedirHandler = NoRedirectHandler()
        withRedirHandler = WithRedirectHandler()
        errHandler = ReqErrorHandler([404, 400, 500, 301, 302, 307, 200])
        errNoRedirHandler = ReqErrorHandler([404, 400, 500, 200])
        self.nofollowOpener = urllib.request.build_opener(errHandler, noRedirHandler)
        self.followOpener = urllib.request.build_opener(withRedirHandler)
        self.followOpener.add_handler(errNoRedirHandler)

        self.unsafeSslContext = ssl.create_default_context()
        self.unsafeSslContext.check_hostname = False
        self.unsafeSslContext.verify_mode = ssl.CERT_NONE
        sslNoVerify = urllib.request.HTTPSHandler(debuglevel=0, context=self.unsafeSslContext, check_hostname=None)

        self.nofollowOpenerNoVerify = urllib.request.build_opener(errHandler, noRedirHandler, sslNoVerify)
        self.followOpenerNoVerify = urllib.request.build_opener(withRedirHandler, sslNoVerify)
        self.followOpenerNoVerify.add_handler(errNoRedirHandler)

    def getOpener(self, follow = True, verify = True):
        if (follow and verify):
            return self.followOpener

        if ((not follow) and verify):
            return self.nofollowOpener

        if (follow and (not verify)):
            return self.followOpenerNoVerify

        if ((not follow) and (not verify)):
            return self.nofollowOpenerNoVerify

    def test__urlOpeners(self):
        print("====== TEST 1 ===========")
        r = self.getOpener(False, True).open('http://cnn.com/', None)
        assert r.code, 301
        print("====== TEST 2 ===========")
        r = self.getOpener(True, True).open('http://fb.com/Belousoff')
        assert r.code, 404
        print("====== TEST 3 ===========")
        r =self.getOpener(False, False).open('https://wrong.host.badssl.com/')
        assert r.code, 200
        print("TEST 3 Pass")
        print("====== TEST 4 ===========")
        r = None
        try:
            r = self.getOpener(False, True).open('https://wrong.host.badssl.com/')
            assert r, None
        except Exception as e:
            print("ex: {}, {}".format(e, type(e)))
            assert type(e), urllib.error.URLError
        print("TEST 4 Pass")

    # Static
    def self_test():
        o = UrlOpener()
        o.test__urlOpeners()

#urllib2.urlopen("https://your-test-server.local", context=ctx)

